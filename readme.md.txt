Циклы нужны для выполнения одного и того же кода многократно, 
например когда требуется вывести длинный спиок, перебрать массивы, вывести карточки товаров и т.д. 
То есть для каждого элемента будет выполенн один и тот же код, что бы не пришлось перебирть его вручную.

Циклы for используются, когда известно количество иттераций, циклы while используются, когда не извесстно количество иттераций.
Циклы do-while используются, когда первая иттерация должна выполниться в любом случае.
